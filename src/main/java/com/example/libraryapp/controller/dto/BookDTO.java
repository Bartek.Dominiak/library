package com.example.libraryapp.controller.dto;

import com.example.libraryapp.domain.model.Book;
import com.example.libraryapp.domain.model.BookCategory;

import java.io.Serializable;

public class BookDTO implements Serializable {

    private Long id;

    private Long ISBN;

    private String title;

    private String author;

    private BookCategory category;

    private Integer yearOfPublication;

    private Boolean isPresent;

    public BookDTO() {
    }

    public BookDTO(Book book) {
        this.id = book.getId();
        this.ISBN = book.getISBN();
        this.title = book.getTitle();
        this.author = book.getAuthor();
        this.category = book.getCategory();
        this.yearOfPublication = book.getYearOfPublication();
        this.isPresent = book.getPresent();
    }

    public Long getISBN() {
        return ISBN;
    }

    public void setISBN(Long ISBN) {
        this.ISBN = ISBN;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public BookCategory getCategory() {
        return category;
    }

    public void setCategory(BookCategory category) {
        this.category = category;
    }

    public Integer getYearOfPublication() {
        return yearOfPublication;
    }

    public void setYearOfPublication(Integer yearOfPublication) {
        this.yearOfPublication = yearOfPublication;
    }

    public Boolean getPresent() {
        return isPresent;
    }

    public void setPresent(Boolean present) {
        isPresent = present;
    }

    public Book toDomain() {
        return new Book(id, ISBN, title, author, category, yearOfPublication, isPresent);
    }
}
