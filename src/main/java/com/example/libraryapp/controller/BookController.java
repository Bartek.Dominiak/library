package com.example.libraryapp.controller;


import com.example.libraryapp.controller.dto.BookDTO;
import com.example.libraryapp.controller.excpetion.NotFoundException;
import com.example.libraryapp.controller.excpetion.OKException;
import com.example.libraryapp.domain.model.Book;
import com.example.libraryapp.domain.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/library")
public class BookController {

    private final BookService bookService;

    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping(path = "/{id}")
    public BookDTO getBook(@PathVariable("id") Long id) {
        Optional<Book> bookOptional = bookService.getBook(id);
        if (!bookOptional.isPresent()) {
            throw new NotFoundException();
        }
        return new BookDTO(bookOptional.get());
    }

    @PostMapping
    public Long creatBook(@RequestBody BookDTO bookDTO) {
        return bookService.creatBook(
                bookDTO.getISBN(),
                bookDTO.getTitle(),
                bookDTO.getAuthor(),
                bookDTO.getCategory(),
                bookDTO.getYearOfPublication(),
                bookDTO.getPresent()
        );
    }

    @GetMapping(path = "/all")
    public List<BookDTO> getAllBooks() {
        List<Book> bookList = bookService.getAllBooks();
        List<BookDTO> bookDTOS = new ArrayList<>();
        for (Book book : bookList) {
            bookDTOS.add(new BookDTO(book));
        }

        return bookDTOS;
    }

    @DeleteMapping(path = "/{id}")
    public void removeBook(@PathVariable("id") Long id) {
        if (bookService.getBook(id).isPresent()) {
            bookService.removeBook(id);
        } else {
            throw new OKException();
        }
    }

    @PutMapping(path = "/update/{id}")
    public void updateBook(@PathVariable("id") Long id, @RequestBody BookDTO bookDTO) {
        if (!bookService.getBook(id).isPresent()) {
            throw new NotFoundException();
        }
        bookService.updateBook(
                id,
                bookDTO.getISBN(),
                bookDTO.getTitle(),
                bookDTO.getAuthor(),
                bookDTO.getCategory(),
                bookDTO.getYearOfPublication(),
                bookDTO.getPresent()
        );
    }

    @PutMapping(path = "/borrow/{id}")
    public void borrowBook(@PathVariable("id") Long id) {
        if (!bookService.getBook(id).isPresent()){
            throw new NotFoundException();
        }
        bookService.borrowBook(id);
    }

    @PutMapping(path = "/return/{id}")
    public void returnBook(@PathVariable("id") Long id) {
        if (!bookService.getBook(id).isPresent()){
            throw new NotFoundException();
        }
        bookService.returnBook(id);
    }
}
