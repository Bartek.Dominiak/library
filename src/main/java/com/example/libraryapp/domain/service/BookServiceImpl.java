package com.example.libraryapp.domain.service;

import com.example.libraryapp.domain.model.Book;
import com.example.libraryapp.domain.model.BookCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;

    @Autowired
    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public Long creatBook(
            Long ISBN,
            String title,
            String author,
            BookCategory category,
            Integer yearOfPublication,
            Boolean isPresent) {

        Book book = bookRepository.creatBook(
                ISBN,
                title,
                author,
                category,
                yearOfPublication,
                isPresent);

        return book.getId();
    }

    @Override
    public Optional<Book> getBook(Long id) {
        return bookRepository.getBookById(id);
    }

    @Override
    public List<Book> getAllBooks() {
        return bookRepository.getAllBook();
    }

    @Override
    public void updateBook(Long id, Long ISBN, String title, String author, BookCategory category, Integer yearOfPublication, Boolean isPresent) {
        bookRepository.updateBook(id, ISBN, title, author, category, yearOfPublication, isPresent);
    }

    @Override
    public void removeBook(Long ISBN) {
        bookRepository.removeBook(ISBN);

    }

    @Override
    public void borrowBook(Long id) {
        bookRepository.borrowBook(id);
    }

    @Override
    public void returnBook(Long id) {
        bookRepository.returnBook(id);
    }
}
