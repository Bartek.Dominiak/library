package com.example.libraryapp.domain.service;

import com.example.libraryapp.domain.model.Book;
import com.example.libraryapp.domain.model.BookCategory;

import java.util.List;
import java.util.Optional;

public interface BookService {

    Long creatBook(
            Long ISBN,
            String title,
            String author,
            BookCategory category,
            Integer yearOfPublication,
            Boolean isPresent
    );

    Optional<Book> getBook(Long id);

    List<Book> getAllBooks();

    void updateBook(Long id, Long ISBN, String title, String author, BookCategory category, Integer yearOfPublication, Boolean isPresent);

    void removeBook(Long id);

    void borrowBook(Long id);

    void returnBook(Long id);
}
