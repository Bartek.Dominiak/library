package com.example.libraryapp.domain.service;

import com.example.libraryapp.domain.model.Book;
import com.example.libraryapp.domain.model.BookCategory;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface BookRepository {

    Book creatBook(
            Long ISBN,
            String title,
            String author,
            BookCategory category,
            Integer yearOfPublication,
            Boolean isPresent
    );

    Optional<Book> getBookByISBN(Long ISBN);

    Optional<Book> getBookById(Long id);


    List<Book> getAllBook();

    void borrowBook(Long id);

    void returnBook(Long id);


    void updateBook(
            Long id,
            Long ISBN,
            String title,
            String author,
            BookCategory category,
            Integer yearOfPublication,
            Boolean isPresent);

    void removeBook(Long id);


}
