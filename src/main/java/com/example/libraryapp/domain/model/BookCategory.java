package com.example.libraryapp.domain.model;

public enum BookCategory {
    HORROR,
    ROMANCE,
    DRAMA,
    CRIME,
    THRILLER,
    FANTASY,
    SCIENCE_FICTION
    }
