package com.example.libraryapp.domain.model;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

public class Book {

    private Long id;

    private Long ISBN;

    private String title;

    private String author;

    private BookCategory category;

    private Integer yearOfPublication;

    private Boolean isPresent;

    public Book() {
    }

    public Book(Long id, Long ISBN, String title, String author, BookCategory category, Integer yearOfPublication, Boolean isPresent) {
        this.id = id;
        this.ISBN = ISBN;
        this.title = title;
        this.author = author;
        this.category = category;
        this.yearOfPublication = yearOfPublication;
        this.isPresent = isPresent;
    }

    public Long getId() {
        return id;
    }

    public Long getISBN() {
        return ISBN;
    }

    public void setISBN(Long ISBN) {
        this.ISBN = ISBN;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public BookCategory getCategory() {
        return category;
    }

    public void setCategory(BookCategory category) {
        this.category = category;
    }

    public Integer getYearOfPublication() {
        return yearOfPublication;
    }

    public void setYearOfPublication(Integer yearOfPublication) {
        this.yearOfPublication = yearOfPublication;
    }

    public Boolean getPresent() {
        return isPresent;
    }

    public void setPresent(Boolean present) {
        isPresent = present;
    }
}
