package com.example.libraryapp.infrastructrure.repository;

import com.example.libraryapp.domain.model.BookCategory;
import com.example.libraryapp.infrastructrure.entity.BookHibernate;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface BookHibernateRepository extends CrudRepository<BookHibernate, Long> {

    List<BookHibernate> findByCategory(BookCategory category);

    List<BookHibernate> findByTitleContaining(String partOfTitle);

    Optional<BookHibernate> findByISBN(Long ISBN);

    @Override
    List<BookHibernate> findAll();

}
