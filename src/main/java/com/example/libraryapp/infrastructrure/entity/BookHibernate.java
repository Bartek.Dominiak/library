package com.example.libraryapp.infrastructrure.entity;


import com.example.libraryapp.domain.model.BookCategory;

import javax.persistence.*;
import java.util.Objects;
import java.util.StringJoiner;

@Entity
@Table(name = "book")
public class BookHibernate {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private Long ISBN;

    private String title;

    private String author;

    @Enumerated(EnumType.STRING)
    private BookCategory category;

    private Integer yearOfPublication;

    private Boolean isPresent;

    public BookHibernate() {
    }

    public BookHibernate(
            Long id,
            Long ISBN,
            String title,
            String author,
            BookCategory category,
            Integer yearOfPublication,
            Boolean isPresent) {
        this.id = id;
        this.ISBN = ISBN;
        this.title = title;
        this.author = author;
        this.category = category;
        this.yearOfPublication =yearOfPublication;
        this.isPresent =isPresent;
    }

    public Long getId() {
        return id;
    }

    public Long getISBN() {
        return ISBN;
    }

    public void setISBN(Long ISBN) {
        this.ISBN = ISBN;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public BookCategory getCategory() {
        return category;
    }

    public void setCategory(BookCategory category) {
        this.category = category;
    }

    public Integer getYearOfPublication() {
        return yearOfPublication;
    }

    public void setYearOfPublication(Integer yearOfPublication) {
        this.yearOfPublication = yearOfPublication;
    }

    public Boolean getPresent() {
        return isPresent;
    }

    public void setPresent(Boolean present) {
        isPresent = present;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookHibernate that = (BookHibernate) o;
        return Objects.equals(ISBN, that.ISBN);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ISBN);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", BookHibernate.class.getSimpleName() + "[", "]")
                .add("ISBN=" + ISBN)
                .add("title='" + title + "'")
                .add("author='" + author + "'")
                .add("category=" + category)
                .add("yearOfPublication=" + yearOfPublication)
                .add("isPresent=" + isPresent)
                .toString();
    }
}


