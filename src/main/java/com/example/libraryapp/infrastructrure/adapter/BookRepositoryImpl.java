package com.example.libraryapp.infrastructrure.adapter;

import com.example.libraryapp.domain.model.Book;
import com.example.libraryapp.domain.model.BookCategory;
import com.example.libraryapp.domain.service.BookRepository;
import com.example.libraryapp.infrastructrure.entity.BookHibernate;
import com.example.libraryapp.infrastructrure.repository.BookHibernateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
public class BookRepositoryImpl implements BookRepository {

    private final BookHibernateRepository bookHibernateRepository;

    @Autowired
    public BookRepositoryImpl(BookHibernateRepository bookHibernateRepository) {
        this.bookHibernateRepository = bookHibernateRepository;
    }


    public Book toDomain(BookHibernate bookHibernate) {
        return new Book(
                bookHibernate.getId(),
                bookHibernate.getISBN(),
                bookHibernate.getTitle(),
                bookHibernate.getAuthor(),
                bookHibernate.getCategory(),
                bookHibernate.getYearOfPublication(),
                bookHibernate.getPresent()
        );
    }


    @Override
    public Book creatBook(
            Long ISBN,
            String title,
            String author,
            BookCategory category,
            Integer yearOfPublication,
            Boolean isPresent) {
        BookHibernate bookHibernate = new BookHibernate(
                null,
                ISBN,
                title,
                author,
                category,
                yearOfPublication,
                isPresent
        );
        bookHibernateRepository.save(bookHibernate);
        return toDomain(bookHibernate);
    }

    @Override
    public Optional<Book> getBookByISBN(Long ISBN) {
        return bookHibernateRepository.findByISBN(ISBN).map(this::toDomain);
    }

    @Override
    public Optional<Book> getBookById(Long id) {
        return bookHibernateRepository.findById(id).map(this::toDomain);
    }

    @Override
    public List<Book> getAllBook() {
        return bookHibernateRepository.findAll().stream().map(this::toDomain).collect(Collectors.toList());
    }

    @Override
    public void borrowBook(Long id) {
        BookHibernate bookHibernate = bookHibernateRepository.findById(id).get();
        bookHibernate.setPresent(false);
        bookHibernateRepository.save(bookHibernate);
    }

    @Override
    public void returnBook(Long id) {
        BookHibernate bookHibernate = bookHibernateRepository.findById(id).get();
        bookHibernate.setPresent(true);
        bookHibernateRepository.save(bookHibernate);
    }

    @Override
    public void updateBook(Long id, Long ISBN, String title, String author, BookCategory category, Integer yearOfPublication, Boolean isPresent) {
        BookHibernate bookHibernate = bookHibernateRepository.findById(id).get();
        bookHibernate.setTitle(title);
        bookHibernate.setAuthor(author);
        bookHibernate.setCategory(category);
        bookHibernate.setYearOfPublication(yearOfPublication);
        bookHibernate.setPresent(isPresent);

        bookHibernateRepository.save(bookHibernate);
    }

    @Override
    public void removeBook(Long id) {
        bookHibernateRepository.deleteById(id);
    }
}
