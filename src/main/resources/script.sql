CREATE TABLE book (
book_id bigint NOT NULL,
isbn bigint NOT NULL,
title varchar (255) NOT NULL,
author varchar (180) NOT NULL,
category varchar (50) NOT NULL,
yearOfPublication int NOT NULL,
is_Present boolean,
PRIMARY KEY (book_id)
);